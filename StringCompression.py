#THE BIG-O IS O(n)
from collections import OrderedDict as compression

def stringCompression(input): 
   
    #create an ordered pair starting at positon 0
    temp = compression.fromkeys(input, 0) 
   
    #Iterate through string to find frequency
    for ch in input: 
        temp[ch] += 1
        #print(ch)
  
    
    output = '' 
    #add the character (key) followed by the frequency (value)
    for key,value in temp.items(): 
         output = output + key + str(value) 
    return output 
   

if __name__ == "__main__": 
    input="aaabbbcccaaa"
    input2="bbcceeee"
    input3="a"
    print (stringCompression(input))
    print (stringCompression(input2))
    print (stringCompression(input3)) 